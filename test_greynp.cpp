#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille,temp1,temp2;
  
  if (argc < 4) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm Seuil1 Seuil2 \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue);
   sscanf (argv[2],"%s",cNomImgEcrite);
   

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   //   for (int i=0; i < nTaille; i++)
   // {
   //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
   //  }


 for (int i=0; i < nH; i++)
   for (int j=0; j < nW; j++)
   {
     sscanf (argv[3],"%d",&temp1);
     if(ImgIn[i*nW+j] < temp1)
     {
       ImgOut[i*nW+j] = 0;
     }
     sscanf (argv[argc-1],"%d",&temp2);
     if(ImgIn[i*nW+j] > temp2)
     {
       ImgOut[i*nW+j] = 255;
     }
      for(int a = 4; a <= argc-1; a++)
      {
        sscanf (argv[a-1],"%d",&temp1);
        sscanf (argv[a],"%d",&temp2);
        if(ImgIn[i*nW+j] <= temp2)
        {
          ImgOut[i*nW+j] = (temp1 + temp2)/2;
          break;
        }
      }
   }
   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
